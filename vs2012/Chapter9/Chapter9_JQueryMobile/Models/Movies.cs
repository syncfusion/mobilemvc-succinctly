﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Globalization;
using System.Web.Mvc;
using System.Web.Security;

namespace Chapter9_jQueryMobile.Models
{
  public class Movie
  {
    [Key]
    public int MovieId { get; set; }
    public string MovieName { get; set; }
    public int YearPublished { get; set; }
    public string Genre { get; set; }
    public string MovieIconURL { get; set; }
    public string MovieDscr { get; set; }
    public Movie() { }
    
    public Movie(int _MovieId, string _MovieName, string _MovieDscr, string _Genre, int _YearPublished, string _MovieIconURL)
    {
      MovieId = _MovieId;
      MovieName = _MovieName;
      Genre = _Genre;
      YearPublished = _YearPublished;
      MovieDscr = _MovieDscr;
      MovieIconURL = _MovieIconURL;
    }
  }
}
